espa-nol (1.11-20) unstable; urgency=medium

  * Enable creation of dictionaries in .bdic format.
  * debian/copyright: Update Debian section year to 2022.
  * Update some lintian overrides to new format.
  * Bump Standards-Version. No changes required.

 -- Agustin Martin Domingo <agmartin@debian.org>  Tue, 13 Dec 2022 22:49:52 +0100

espa-nol (1.11-19) unstable; urgency=medium

  * debian/rules: fix word count for myspell dict.

 -- Agustin Martin Domingo <agmartin@debian.org>  Wed, 13 Oct 2021 18:28:27 +0200

espa-nol (1.11-18) unstable; urgency=medium

  * Re-add myspell-es symlinks.

 -- Agustin Martin Domingo <agmartin@debian.org>  Mon, 04 Oct 2021 09:34:08 +0200

espa-nol (1.11-17) unstable; urgency=medium

  [ Debian Janitor ]
  * Use secure URI in Homepage field.
  * Bump debhelper from old 12 to 13.

  [ Agustin Martin Domingo ]
  * Use dh_* sequencer.
  * debian/{LEAME.Debian,README.Debian,README.MySpell_es_ES}: Recode to
    UTF-8.
  * Add some lintian overrides.
  * debian/control:
    - Add Rules-Requires-Root: no.
    - Bump Standards-Version. No changes required.
  * debian/watch: Use https for upstream url.
  * debian/copyright: Make DEP5.
  * debian/ispanish.links: no need to add usr/lib/ispell/espa~nol.hash ->
    var/lib/ispell/espa~nol.hash symlink, it is handled by
    ispell-autobuildhash.

 -- Agustin Martin Domingo <agmartin@debian.org>  Sun, 03 Oct 2021 23:07:48 +0200

espa-nol (1.11-16) unstable; urgency=low

  [ Debian Janitor ]
  * Bump debhelper from old 11 to 12.
  * Set debhelper-compat version in Build-Depends.

 -- Agustin Martin Domingo <agmartin@debian.org>  Mon, 28 Dec 2020 17:07:54 +0100

espa-nol (1.11-15.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Fri, 25 Dec 2020 18:09:20 +0100

espa-nol (1.11-15) unstable; urgency=medium

  * debian/control:
    - Update Vcs-Browser and Vcs-Git for salsa migration.
    - Bump Standards-Version. No changes required.
  * Bump debhelper compat level to 11.

 -- Agustin Martin Domingo <agmartin@debian.org>  Fri, 04 May 2018 16:53:15 +0200

espa-nol (1.11-14) unstable; urgency=medium

  * debian/control::myspell-es: Use Conflicts where needed.

 -- Agustin Martin Domingo <agmartin@debian.org>  Tue, 22 Sep 2015 12:29:05 +0200

espa-nol (1.11-13) unstable; urgency=medium

  * debian/control:: myspell-es: Provide hunspell counterparts.

 -- Agustin Martin Domingo <agmartin@debian.org>  Mon, 21 Sep 2015 19:04:49 +0200

espa-nol (1.11-12) unstable; urgency=medium

  * debian/control: Break possible hunspell-es to avoid name collisions.

 -- Agustin Martin Domingo <agmartin@debian.org>  Mon, 21 Sep 2015 15:18:29 +0200

espa-nol (1.11-11) unstable; urgency=medium

  * debian/rules: Make sure build is always done in a reproducible and
    working LC_ALL=C language environment

 -- Agustin Martin Domingo <agmartin@debian.org>  Thu, 18 Jun 2015 20:19:21 +0200

espa-nol (1.11-10) unstable; urgency=medium

  * debian/rules: Make the build reproducible.
    Thanks Chris Lamb (Closes: #779162).

 -- Agustin Martin Domingo <agmartin@debian.org>  Thu, 09 Apr 2015 17:29:31 +0200

espa-nol (1.11-9) unstable; urgency=low

  * debian/control:
    - Make aspell-es "Multi-Arch: foreign" (See aspell-en #763892).
    - Update Vcs-Browser to cgit address.
    - Bump Standards-Version. No changes required.

 -- Agustin Martin Domingo <agmartin@debian.org>  Mon, 06 Oct 2014 16:17:32 +0200

espa-nol (1.11-8) unstable; urgency=low

  * Do not ship /var/lib/{a,i}spell dirs nor related symlinks.
  * Rebuild against sid dictionaries-common-dev 1.23.2 to properly clean
    dirs.

 -- Agustin Martin Domingo <agmartin@debian.org>  Thu, 24 Apr 2014 11:31:23 +0200

espa-nol (1.11-7) unstable; urgency=low

  * Rebuild against sid dictionaries-common-dev 1.23.0.
  * debian/rules: Create es.cwl.gz in a single line (LP: #73304).

 -- Agustin Martin Domingo <agmartin@debian.org>  Wed, 09 Apr 2014 15:35:51 +0200

espa-nol (1.11-6~exp1) experimental; urgency=low

  * Rebuild against experimental dictionaries-common-dev.

 -- Agustin Martin Domingo <agmartin@debian.org>  Thu, 06 Mar 2014 20:09:40 +0100

espa-nol (1.11-5) unstable; urgency=low

  * debian/control:
    - Modify to use dictionaries-common-dev (>= 1.22.0) features:
      + ${ispell:Depends}, ${aspell:Depends}, ${hunspell:Depends}
      + Deal with remove files in debhelper snippet.
    - Bump Standards Version. No changes required.
    - Canonicalize Vcs-* headers.
    - Change versioned Conflicts to Breaks.
  * Add debian/source/format for 3.0 (quilt). Add empty series file.
  * debian/watch: Remove dh_make header.
  * debian/aspell-es.links: Recode to UTF-8.
  * debian/ispanish.postinst: Explicitly use 'set -e'

 -- Agustin Martin Domingo <agmartin@debian.org>  Wed, 05 Mar 2014 12:36:13 +0100

espa-nol (1.11-4) unstable; urgency=low

  * No longer build depend on obsolete dpatch package.

 -- Agustin Martin Domingo <agmartin@debian.org>  Mon, 06 Feb 2012 19:12:24 +0100

espa-nol (1.11-3) unstable; urgency=low

  * debian/rules:
    - Fix lintian debian-rules-missing-recommended-target build-{arch,indep}.
  * debian/control:
    - Improve myspell-eo suggestions line and add libreoffice.
    - Does not need to provide hunspell dictionary.

 -- Agustin Martin Domingo <agmartin@debian.org>  Wed, 14 Sep 2011 13:28:47 +0200

espa-nol (1.11-2) unstable; urgency=low

  * Build-depend on at least dictionaries-common-dev (>= 1.11.2):
    - Stop calling update-openoffice-dicts (Closes: #628810).
    - Use new dictionaries-common-dev auto-compat feature.
  * Bump Standards version. No changes required.

 -- Agustin Martin Domingo <agmartin@debian.org>  Thu, 02 Jun 2011 18:21:19 +0200

espa-nol (1.11-1) unstable; urgency=low

  * New upstream version.
  * Bump Standards-Version. No changes needed.

 -- Agustin Martin Domingo <agmartin@debian.org>  Thu, 18 Nov 2010 20:28:29 +0100

espa-nol (1.10-9) unstable; urgency=low

  * Make sure we no longer create Mozilla* symlinks in new destdir
    by using installdeb-myspell (> 1.4).
  * Install myspell dict as es.{dic,aff} and use automatic symlink
    creation from installdeb-myspell (> 1.3.2)
  * myspell-es.info-hunspell: Use plain 'es' as hashname.

 -- Agustin Martin Domingo <agmartin@debian.org>  Wed, 25 Nov 2009 12:23:09 +0100

espa-nol (1.10-8) unstable; urgency=low

  * Rebuild against dictionaries-common-dev 1.3.1 to have
    mozilla symlinks fixed.

 -- Agustin Martin Domingo <agmartin@debian.org>  Tue, 25 Aug 2009 16:50:26 +0200

espa-nol (1.10-7) unstable; urgency=low

  * Bump Standards Version to 3.8.3. No changes required.
  * Install myspell dicts in new location. Use new
    installdeb-myspell (at least 1.3) for this and for
    compatibility symlinks (Closes: #541937).
  * Add debian/README.source pointing to dpatch file.

 -- Agustin Martin Domingo <agmartin@debian.org>  Tue, 25 Aug 2009 15:03:17 +0200

espa-nol (1.10-6) unstable; urgency=low

  * debian/*.info-*spell:
    - Do not explicitly add '-d es' and friends to ispell-args.
      New dictionaries-common will take care of that.
  * debian/control:
    - Build Dep on at least dictionaries-common-dev 1.2 to use
      new debhelper snippets.
  * Raise debhelper compat level to 7. Use dh_prep.
  * Raise Standards-Version to 3.8.1. No changes required.
  * debian/rules: Add explicit myspell links for all spanish variants.

 -- Agustin Martin Domingo <agmartin@debian.org>  Fri, 13 Mar 2009 16:30:50 +0100

espa-nol (1.10-5) unstable; urgency=low

  * Use new dictionaries-common registration system for
    myspell/hunspell dictionaries.
  * Provide hunspell-es and hunspell dictionary.
  * Remove ancient openoffice.org-updatedicts or-dependency.
  * Add ${misc:Depends} where needed.

 -- Agustin Martin Domingo <agmartin@debian.org>  Tue, 17 Feb 2009 00:54:57 +0100

espa-nol (1.10-4) unstable; urgency=low

  * debian/control:
    - Bumped Standards-Version to 3.8.0. No changes required
    - Reorganize Build-Depends-Indep for better readability.
  * debian/rules:
    - Use 'prezip -s' instead of explicit 'LC_COLLATE=C sort -u' for
      aspell dict.
    - Use best gzip compression.
  * debian/watch: Updated to new structure of upstream www site.

 -- Agustin Martin Domingo <agmartin@debian.org>  Wed, 01 Oct 2008 14:54:48 +0200

espa-nol (1.10-3) unstable; urgency=low

  * debian/control: Added Homepage field.
  * Updated version and dates in info, README and copyright files.

 -- Agustin Martin Domingo <agmartin@debian.org>  Mon, 09 Jun 2008 14:10:46 +0200

espa-nol (1.10-1) unstable; urgency=low

  * New Upstream Version
  * debian/control: Added vcs fields

 -- Agustin Martin Domingo <agmartin@debian.org>  Thu, 22 May 2008 14:52:35 +0200

espa-nol (1.9-16) unstable; urgency=low

  * debian/aspell/info:
    - Modified for new proc script.
    - Minor fixes for official aspell dict
  * debian/control:
    - Removed authors e-mails. That belongs to copyright or README
  * debian/copyright, debian/aspell/{Copyright,info}:
    - Updated authors e-mails

 -- Agustin Martin Domingo <agmartin@debian.org>  Mon, 07 Apr 2008 13:53:21 +0200

espa-nol (1.9-15) unstable; urgency=low

  * debian/control:
    - Updated to use the new iceape-browser, iceweasel, icedove names.
  * debian/copyright:
    - Minor changes

 -- Agustin Martin Domingo <agmartin@debian.org>  Mon, 03 Mar 2008 15:36:23 +0100

espa-nol (1.9-14) unstable; urgency=low

  * debian/control:
    - Bumped standards version to 3.7.3. No changes needed
  * Move reconfigure compat initialization from config to
    postinst. debian/aspell-es.overrides is no longer needed.
  * Removed trailing whitespace in some places.
  * debian/rules:
    - Removed binary-arch target.
    - Fixed harmless error in cwl creation.
  * debian/ispanish.dirs: Do not create usr/share/doc/ispell
  * debian/*.info-{a,i}spell: Do not put dash in otherchars

 -- Agustin Martin Domingo <agmartin@debian.org>  Mon, 14 Jan 2008 15:02:12 +0100

espa-nol (1.9-13) unstable; urgency=low

  * debian/watch:
    - Readded a fixed watch file

 -- Agustin Martin Domingo <agmartin@debian.org>  Wed, 07 Nov 2007 14:22:13 +0100

espa-nol (1.9-12) unstable; urgency=low

  * debian/aspell-es.links:
    - Added a castellano8 alias

 -- Agustin Martin Domingo <agmartin@debian.org>  Wed, 17 Oct 2007 13:49:33 +0200

espa-nol (1.9-11) unstable; urgency=low

  * debian/control::Build-Depends-Indep:
    - Added alternative dependency on myspell-tools

 -- Agustin Martin Domingo <agmartin@debian.org>  Fri, 29 Jun 2007 11:55:16 +0200

espa-nol (1.9-10) unstable; urgency=low

  * debian/control::Build-Depends-Indep:
    - hunspell-tools is added and preferred over libmyspell-dev.
      ispellaff2myspell is also there in sid.

 -- Agustin Martin Domingo <agmartin@debian.org>  Mon, 21 May 2007 16:58:56 +0200

espa-nol (1.9-9) unstable; urgency=low

  * debian/rules, debian/aspell/*:
    - Fine tuned official aspell dict creation to use aspell proc script.
  * debian/es_ES.myheader: Updated copyright years
  * Removed useless watch file

 -- Agustin Martin Domingo <agmartin@debian.org>  Tue, 13 Feb 2007 13:09:24 +0100

espa-nol (1.9-8) unstable; urgency=medium

  * debian/{control,rules}: Add dpatch support
  * debian/patches/:
    - 500_espa~nol.aff_fix-v-flag.dpatch: new patch for the espa~nol aff file
  * debian/rules, debian/aspell/*:
    - Preliminary support for creating official aspell dict.
  * debian/rules:
    - Do not create md5sums for var/lib/aspell and var/lib/ispell
      contents.

 -- Agustin Martin Domingo <agmartin@debian.org>  Wed, 29 Nov 2006 13:27:39 +0100

espa-nol (1.9-7) unstable; urgency=low

  * debian/control:
    - Add missing "the" and uniformize descriptions (closes: #383906)

 -- Agustin Martin Domingo <agmartin@debian.org>  Wed,  6 Sep 2006 11:31:48 +0200

espa-nol (1.9-6) unstable; urgency=low

  * debian/control:
    - Moved debhelper to Build-Depends. It is required to run the
      clean target of debian/rules and therefore must be listed in
      Build-Depends, even if no architecture-dependent packages
      are built (Policy Manual, section 7.6). Thanks lintian for
      the check and Pierre Habouzit for reminding.
    - Raise Standards-Version to 3.7.2. No changes required.

 -- Agustin Martin Domingo <agmartin@debian.org>  Fri, 16 Jun 2006 14:09:38 +0200

espa-nol (1.9-5) unstable; urgency=low

  * debian/{aspell-es.config,aspell-es.overrides,rules}: Simplify code
    and install overrides file.
  * Copyright notice improved and updated
  * Updated watch file.

 -- Agustin Martin Domingo <agmartin@debian.org>  Wed, 19 Apr 2006 13:14:48 +0200

espa-nol (1.9-3) unstable; urgency=low

  * debian/rules: make sure single chars from espa~nol.words are
    passed to icombine, munchlist is stripping them (closes: #358447)

 -- Agustin Martin Domingo <agmartin@debian.org>  Thu, 23 Mar 2006 19:58:52 +0100

espa-nol (1.9-2) unstable; urgency=low

  * debian/rules: No longer make ispellaff2myspell split flags.
    aspell and hunspell do not like that.

 -- Agustin Martin Domingo <agmartin@debian.org>  Thu, 16 Mar 2006 19:09:49 +0100

espa-nol (1.9-1) unstable; urgency=low

  * New upstream version

 -- Agustin Martin Domingo <agmartin@debian.org>  Wed, 16 Nov 2005 14:22:11 +0100

espa-nol (1.8-7) unstable; urgency=low

  * debian/control:
    - Fix myspell-es suggestions, to keep debcheck QA page happy

 -- Agustin Martin Domingo <agmartin@debian.org>  Fri, 16 Sep 2005 16:33:51 +0200

espa-nol (1.8-6) unstable; urgency=low

  * Added Aspell-Locales section to debian/aspell-es.info-aspell.

 -- Agustin Martin Domingo <agmartin@debian.org>  Tue, 13 Sep 2005 15:12:41 +0200

espa-nol (1.8-5) unstable; urgency=low

  * debian/control:
    - Add "| debconf-2.0" to ispanish debconf dependency
      to unblock installation of cdebconf

 -- Agustin Martin Domingo <agmartin@debian.org>  Thu,  1 Sep 2005 17:00:49 +0200

espa-nol (1.8-4) unstable; urgency=low

  * Bumped version to override a wrong aspell-es upload to really change
    aspell-es Maintaner name.

 -- Agustin Martin Domingo <agmartin@debian.org>  Tue, 12 Jul 2005 16:13:09 +0200

espa-nol (1.8-3) unstable; urgency=low

  * aspell-es merged into espa-nol source package, and modified
    to use aspell-autobuildhash and aspell (>= 0.60.3-2).
    Maintainer for aspell-es changed in this process (closes: #317444):
    - debian/control:
      · Added aspell-es with arch: all, provides aspell-dictionary and
        depends on dictionaries-common (>= 0.40) and aspell (>> 0.60.3-2)
    - debian/rules:
      · Adapted for aspell-es and aspell-autobuildhash
    - debian/aspell-es.{dirs,links,info-aspell}, debian/es.dat:
      · New files needed by aspell-es
    - debian/aspell-es.config:
      · Make sure hashes are committed for rebuilding when package
        is reconfigured

 -- Agustin Martin Domingo <agmartin@debian.org>  Tue, 12 Jul 2005 13:04:33 +0200

espa-nol (1.8-2) unstable; urgency=low

  * Using replacements table for myspell. Updated the table
  * debian/es_ES.aff file is no longer used. Removed.
  * added debian/compat file with level 4, and removed obsolete
    compat strings from debian/rules
  * Raised Standards-version to 3.6.2. No changes required

 -- Agustin Martin Domingo <agmartin@debian.org>  Thu,  7 Jul 2005 16:56:32 +0200

espa-nol (1.8-1) unstable; urgency=low

  * New upstream version (closes: #304899)

 -- Agustin Martin Domingo <agmartin@debian.org>  Mon, 18 Apr 2005 13:06:59 +0200

espa-nol (1.7-27) unstable; urgency=low

  * debian/ispanish.config.in:
    - Removed extra whitespace in an open call. This only became
      evident in woody boxes when reconfiguring the package,
      causing compat file not being opened.

 -- Agustin Martin Domingo <agmartin@debian.org>  Wed,  9 Feb 2005 16:57:26 +0100

espa-nol (1.7-26) unstable; urgency=low

  * Modified to use ispell-autobuildhash. ispanish is now arch: all.
    - debian/{control,rules,ispanish.config.in,ispanish.dirs,ispanish.links}
  * debian/control: Updated libmyspell-dependency to (>=1:3.1-7). This
    way the newest ispellaff2myspell is used.
  * Modified the way charsets are put together into the .aff file.

 -- Agustin Martin Domingo <agmartin@debian.org>  Fri, 29 Oct 2004 13:05:45 +0200

espa-nol (1.7-25) unstable; urgency=low

  * New link as espanol.hash under /usr/lib/ispell
  * Added utf8 support (closes: #254254):
    - New utf8 entry in espa~nol.aff.
    - New utf8 entry in info-ispell file. This is mostly for
      ispell-wrapper benefit and will not be used in emacs,
      jed or debconf prompt.
  * debian/control:
    - Removed spanish task
    - Fixed typo in Build-Depends line, '<' should be '>' (closes: #256306)

 -- Agustin Martin Domingo <agmartin@debian.org>  Mon, 28 Jun 2004 12:03:56 +0200

espa-nol (1.7-22) unstable; urgency=low

  * debian/{README,LEAME}.Debian:
    - Added hints about how to deal with personal dictionaries using
      obsolete spanish-tex format.
  * Moved docs list to {ispanish,myspell-es}.docs
  * Install orig2latin1.sed into the docs dir

 -- Agustin Martin Domingo <agmartin@debian.org>  Wed,  3 Mar 2004 15:59:40 +0100

espa-nol (1.7-21) unstable; urgency=low

  *  debian/myspell-es.info-myspell: Added a list of
     es-COUNTRY entries for countries using spanish
     (closes: #233712)

 -- Agustin Martin Domingo <agmartin@debian.org>  Thu, 19 Feb 2004 19:48:40 +0100

espa-nol (1.7-20) unstable; urgency=low

  * Modified to use ispellaff2myspell. Will now build depend on
    libmyspell-dev (<=1:3.1-5) for this.
  * Using new dictionaries-common config system. Updated ispanish
    dependencies to dictionaries-common (>=0.20) and package
    build-dependencies to dictionaries-common-dev (>=0.20)
    (closes: #232159)

 -- Agustin Martin Domingo <agmartin@debian.org>  Wed, 11 Feb 2004 10:27:42 +0100

espa-nol (1.7-18) unstable; urgency=low

  * debian/control:
    - Make sure package is build with at least
      dictionaries-common 0.16.0.
  * Use installdeb-myspell.
  * debian/ispanish.links:
    - Added new /usr/share/enchant/ispell/espanol.{hash,aff}
      symlink to /usr/lib/ispell/espa~nol.{hash,aff} for enchant
      compatibility
  * debian/ispanish.info-ispell:
    - Do not display obsolete "castellano (Spanish Tex mode)"
      entry at debconf question (using 'Debconf-Display: no' in
      its info-ispell entry).

 -- Agustin Martin Domingo <agmartin@debian.org>  Mon, 15 Dec 2003 13:40:34 +0100

espa-nol (1.7-17) unstable; urgency=low

  * debian/control: Make sure package is build with at least
    dictionaries-common 0.15.5.

 -- Agustin Martin Domingo <agmartin@debian.org>  Mon, 13 Oct 2003 16:06:22 +0200

espa-nol (1.7-16) unstable; urgency=low

  * debian/
    - 'ciencia.words' renamed to 'debian.words'. 'rules' also
      modified for that.
    - Added 'ispanish.postinst' to take care of removal of obsolete
      '/etc/emacs/site-start.d/50ispanish.el' file
    - Recoded 'changelog' to utf8 and added emacs entries to handle
      that. Policy bumped to 3.6.1 in 'control'
    - Raised build dependency to dictionaries-common-dev (>=0.15.3)
      to use last version of config file.

 -- Agustin Martin Domingo <agmartin@debian.org>  Wed, 17 Sep 2003 12:00:32 +0200

espa-nol (1.7-15) unstable; urgency=low

  * debian/control:
    - Fixing wrong section for myspell-es. Should be text instead
      of editors.
  * debian/{ispanish.info-ispell,charsets.debian}:
    - Added a bunch of chars that, even if not used in spanish,
      can be part of foreign words or are usual mistypings with a
      spanish keyboard.

 -- Agustin Martin Domingo <agmartin@debian.org>  Thu, 11 Sep 2003 12:52:48 +0200

espa-nol (1.7-14) unstable; urgency=low

  * Including myspell-es in the ispanish package and made it
    conform to the myspell new policy. This package
    provides/replaces/conflicts with old package
    'openoffice.org-spellcheck-es' making it obsolete.
    (closes: #206085)
  * Changed source name from ispanish to espa-nol to comply
    better with upstream (unfortunately espa~nol is not
    possible). This is better now that we generate more than
    ispanish from these sources.
  * Removing call to dh_installemacsen. This is done by the
    dictionaries-common stuff.

 -- Agustin Martin Domingo <agmartin@debian.org>  Mon,  1 Sep 2003 14:28:50 +0200

ispanish (1.7-12) unstable; urgency=low

  * Just a rebuild to make sure at least dictionaries-common
     0.8.4 is used.

 -- Agustin Martin Domingo <agmartin@debian.org>  Wed, 23 Oct 2002 14:51:25 +0200

ispanish (1.7-11) unstable; urgency=low

  * New policy compliant official package (closes: #163665).
  * Final fine tunings or policy:
    - Lowered 'Pre-Depends: dictionaries-common' to a simple Depends.
    - Removed all the warnings about the experimental character of the
      package
  * Moved ciencia.words to debian subdir and updated debian/rules
    accordingly.
  * Added 'Task: spanish' line to the control file for tasksel use.
  * Removed accents from my name to have a 7bit clean control file.
  * This was really 1.7-9, but what was uploaded to erlangen as 1.7-9
    has a huge diff.gz (434131) while I have here just 1811. This was
    caused by some errors manipulating the cvs. This has also eaten
    1.7-10

 -- Agustin Martin Domingo <agmartin@debian.org>  Tue, 15 Oct 2002 18:54:17 +0200

ispanish (1.7-8) unstable; urgency=low

  * Added a watch file

  (1.7-7) Tue, 11 Jun 2002 17:18:56

  * Added a Pspell-Ispell field to info-ispell file with contents
    'es iso8859-1'. Since we use installdeb-ispell this will autogenerate
    the appropiate pspell-ispell pwli files.

 -- Agustín Martín Domingo <agmartin@debian.org>  Tue, 25 Jun 2002 15:53:59 +0200

ispanish (1.7-6) unstable; urgency=low

  * Added versioning to Build-Depends debhelper entry.
    DH_COMPAT=2 requires at least dephelper (>= 2.0.40).
    Thanks to PÁSZTOR György for noting this

 -- Agustín Martín Domingo <agmartin@debian.org>  Tue,  2 Apr 2002 17:46:17 +0200

ispanish (1.7-5) unstable; urgency=low

  * Rebuilding against ispell 3.1.20-30. Will now use MAXSTRINGCHARS=128

 -- Agustín Martín Domingo <agmartin@debian.org>  Thu, 14 Feb 2002 17:09:30 +0100

ispanish (1.7-4.6) unstable; urgency=low

  * The "typo fixing" release.
  * debian/{changelog,control}: language names should be
    capitalized in English. Thanks, Matt!(closes: #124767)

 -- Agustín Martín Domingo <agmartin@debian.org>  Wed,  9 Jan 2002 15:44:02 +0100

ispanish (1.7-4.5) unstable; urgency=low

  * Rebuilt against new dictionaries-common-dev (0.4.99.7). Code on
    remove|purge has changed
  * Added Additionalchars entries to the info-ispell file
  * debian/control: Upgraded sites for information on policy

 -- Agustín Martín Domingo <agmartin@debian.org>  Thu, 29 Nov 2001 13:08:16 +0100

ispanish (1.7-4.4) unstable; urgency=low

  * Reverted name changes to i*, w*

 -- Agustín Martín Domingo <agmartin@debian.org>  Tue, 27 Nov 2001 18:30:08 +0100

idict-spanish (1.7-4.3) unstable; urgency=low

  * Rebuilt against dictionaries-common 0.4.5
  * Reverted change in the name of the emacsen. It is again
    50idict-spanish.el to test better the emacs menu sorting stuff.

 -- Agustín Martín Domingo <agmartin@debian.org>  Fri, 16 Nov 2001 16:49:43 +0100

idict-spanish (1.7-4.2) unstable; urgency=low

  * Rebuilt against dictionaries-common-dev 0.4.4

 -- Agustín Martín Domingo <agmartin@debian.org>  Mon, 12 Nov 2001 17:42:56 +0100

idict-spanish (1.7-4.1) unstable; urgency=low

  * Rebuilt against new dictionaries-common-dev. "isdefault true"
    to "seen false" change is reverted in debconf db_fset calls.
    "isdefault true" is still active although deprecated and is required
    for the system to work in potato where "seen" do not exists.

 -- Agustín Martín Domingo <agmartin@debian.org>  Tue,  9 Oct 2001 12:22:55 +0200

idict-spanish (1.7-3) unstable; urgency=low

  * Rebuilt against new dictionaries-common-dev. Will change "isdefault
    true" to "seen false" in debconf db_fset calls
  * Added debconf dependency to keep lintian happy, although
    dicionaries-common already have it
  * Raised policy version to 3.5. Seems to require no changes


 -- Agustín Martín Domingo <agmartin@debian.org>  Fri,  7 Sep 2001 15:43:22 +0200

idict-spanish (1.7-2) unstable; urgency=low

  * Rebuilt against new dictionaries-common-dev

 -- Agustín Martín Domingo <agmartin@debian.org>  Thu, 12 Jul 2001 15:53:31 +0200

idict-spanish (1.7-1) unstable; urgency=low

  * New upstream release

 -- Agustín Martín Domingo <agmartin@debian.org>  Tue, 10 Jul 2001 15:29:13 +0200

idict-spanish (1.6-9) unstable; urgency=low

  * debian/idict-spanish.links: New file to let debhelper to take care
    of symlinks. Removed link creation in debian/rules.
  * Put version number in sync with the official branch.

 -- Agustín Martín Domingo <agmartin@debian.org>  Fri, 15 Jun 2001 12:23:50 +0200

idict-spanish (1.6-7.2) unstable; urgency=low

  * A different method to recode to latin1 the .aff file is implemented
  * debian/aff2latin1, debian/charsets.debian and debian/orig2latin1.sed:
    New files for the new latin1 aff building system
  * debian/rules: Modified to use debian/aff2latin1 instead of latin1.pl

 -- Agustín Martín Domingo <agmartin@debian.org>  Fri, 15 Jun 2001 12:22:57 +0200

idict-spanish (1.6-7.1) unstable; urgency=low

  * Moved debian make stuff to debian/rules. Restored original Makefile,
    which is no longer used in the package build process
  * debian/idict-spanish.emacsen-startup: minor text correction
  * debian/LEAME.debian: ispanish changed to idict-spanish
  * Restored original name espa~nol-1.6 in the directory stored in
    idict-spanish_1.6.orig.tar.gz instead of idict-spanish-1.6
  * debian/rules: Moved idict_debinst after dh_installdocs. This should not
    be required after a patch in idict_debinst.

 -- Agustín Martín Domingo <agmartin@debian.org>  Fri, 12 Jan 2001 17:49:42 +0100

idict-spanish (1.6-7) unstable; urgency=low

  * Minor change in emacs startup code to match  castellano and
    castellano8 entries in ispell.el

 -- Agustín Martín Domingo <agmartin@debian.org>  Thu, 16 Nov 2000 19:17:47 +0100

idict-spanish (1.6-6) unstable; urgency=low

  * Added debhelper build dependency
    (Closes: Bug #68938) [ispanish_1.6-5(unstable): Missing build dependencies]
  * Now the package will create only idict-spanish. Removed all
    references to ispanish.
  * Now the entries in prerm and postinst will be generated by the
    script idict_debinst. Included dictionaries-common in build
    dependencies.
  * Removed old postinst and prerm scripts. Now debhelper and idict-debinst
    generate them.
  * Raised policy to 3.1.0

 -- Agustín Martín Domingo <agmartin@debian.org>  Tue, 12 Sep 2000 16:45:25 +0200

idict-spanish (1.6-5) unstable; urgency=low

  * debian/rules: swap the names binary-indep and binary-arch, which were
    not swaped after changing architecture to any. This allows autobuilds for
    architectures other than i386
    Closes 67394 [ispanish_1.6-4(unstable): wrong targets in debian/rules]

 -- Agustín Martín Domingo <agmartin@aq.upm.es>  Wed,  9 Aug 2000 12:04:48 +0200

idict-spanish (1.6-4pre1) unstable; urgency=low

  * Renamed source package to idict-spanish, to make tests of
    dictionaries complying with the pre-proposal for ispell
    dictionaries policy. Source package now builds two dictionary
    packages, one in the old style (ispanish) and one in the new
    style (idict-spanish)
  * Installed dictionary as spanish.{aff,hash} and set links
    espa~nol.{aff,hash} and castellano.{aff,hash} pointing to it
    to comply with above pre-proposal

 -- Agustín Martín Domingo <agmartin@aq.upm.es>  Wed, 14 Jun 2000 13:26:00 +0200

ispanish (1.6-4) unstable; urgency=low

  * 50ispanish.el: Added (ispell-change-dictionary) to make emacs20
    rebuild ispell menus with available dictionaries. Do not work with
    emacs19 or xemacs21

 -- Agustín Martín Domingo <agmartin@aq.upm.es>  Wed, 14 Jun 2000 13:25:53 +0200

ispanish (1.6-3) unstable; urgency=low

  * Makefile.debian: Now using icombine to mix espa~nol.words+ from
    upstream and debian local aditions. This results in a much faster
    processing, and forces to remove some changes done in the last
    release
  * Removed some unneeded files from source
  * Moved install stuff from debian/rules to Makefile.Debian
  * debian/control: Changed architecture from all to any to prevent
    possible low/big endian problems in the binary hash file
  * debian/control: Added Build-Depends: ispell
    Raised policy to 3.1.0
  * 'case' structure removed from postinst and preinst and substituted
    by code adapted from the portuguese dictionary.

 -- Agustín Martín Domingo <agmartin@aq.upm.es>  Mon, 29 May 2000 15:21:13 +0200

ispanish (1.6-2) unstable; urgency=low

  * Migrated makefile work to Makefile.Debian
  * Makefile.Debian: Modified to avoid overwriting espa~nol.words+ when
    adding aditional words. Should make the diff file much simpler and
    avoid redundances in it.
  * debian/rules: Adapted to the Makefile.Debian structure
  * Added additional words:
      ciencia.words: Some scientific and technical words in the DRAE or
                     other reference dictionaries.
                     Included in the working dictionary. ( Closes #26875 )
  * Raised Standards version to 3.0.1

 -- Agustín Martín Domingo <agmartin@aq.upm.es>  Mon, 10 Apr 2000 17:46:07 +0200

ispanish (1.6-1) unstable; urgency=low

  * New upstream release

 -- Enrique Zanardi <ezanard@debian.org>  Tue, 21 Mar 2000 12:46:13 +0100

ispanish (1.5-6amd5) unstable; urgency=low

  * 50ispanish.el: Added a check to act only in emacs19. Sometimes there
    appeared two couples of menu entries in emacs20

 -- Enrique Zanardi <ezanard@debian.org>  Thu,  9 Mar 2000 17:35:45 +0100

ispanish (1.5-6amd4) unstable; urgency=low

  * Added link to dict as 'castellano' (Closes #49804)
  * Moved documentation to /usr/share/doc
  * Migrated from debstd to debhelper
  * Changed ispanish.el in /etc/emacs/site-start.d/ to 50ispanish.el,
    and completely rewritten from the original entries for Julio Sánchez
    Dictionary and parts of the Cyrillic Howto. Hope this works better
    for emacs19
  * Modified README.debian and LEAME.debian to add info about the
    castellano link

 -- Enrique Zanardi <ezanard@debian.org>  Wed,  1 Mar 2000 11:07:05 +0100

ispanish (1.5-6) unstable; urgency=low

  * Added a bunch of words. Closes #26875
  * Make sure that latin1.pl is executable. That'll allow autobuilds.

 -- Enrique Zanardi <ezanard@debian.org>  Tue, 23 Feb 1999 13:10:34 +0000

ispanish (1.5-5) unstable; urgency=low

  * Latin1 is now the default formatter (suggested by Salvador Petit).
  * Added /etc/emacs/site-start.d/ispanish.el. That lisp snippet
    registers the spanish dictionary in Emacs dictionary list. fixes: 22293

 -- Enrique Zanardi <ezanard@debian.org>  Fri, 17 Jul 1998 19:06:05 +0100

ispanish (1.5-4) unstable; urgency=low

  * Updated Standards-Version.
  * Added LEAME.debian (README.debian translated to spanish).

 -- Enrique Zanardi <ezanardi@molec1.dfis.ull.es>  Thu, 12 Feb 1998 18:45:19 +0000

ispanish (1.5-3) unstable; urgency=low

  * /usr/doc/ispanish/copyright is not compressed (Closes Bug#14406).

 -- Enrique Zanardi <ezanardi@molec1.dfis.ull.es>  Sun, 25 Jan 1998 04:10:31 +0000

ispanish (1.5-2) unstable; urgency=low

  * Rebuilt hash file with libc6 (Bug#14778).
  * Pristine sources.

 -- Enrique Zanardi <ezanardi@molec1.dfis.ull.es>  Fri, 14 Nov 1997 09:30:29 +0000

ispanish (1.5-1) unstable; urgency=low

  * New upstream release.

 -- Enrique Zanardi <ezanardi@molec1.dfis.ull.es>  Wed, 5 Feb 1997 02:16:18 +0000

ispanish (1.4-1) unstable; urgency=low

  * Initial Release.

 -- Enrique Zanardi <ezanardi@molec1.dfis.ull.es>  Sun, 5 Jan 1997 17:08:48 +0000


Local Variables:
  coding: utf-8
  ispell-local-dictionary: "american"
End:
